<!--
  FOOTER
-->

	<footer>
    
    <div id="footer-main">
      <div class="container">
        <div class="col-xs-6 col-sm-3 col-lg-offset-1 col-lg-2">
          <ul>
            <li><a target="_blank" href="https://phlok.me/partners">Partners</a></li>
            <li><a target="_blank" href="https://phlok.me/privacy">Terms &amp; Privacy</a></li>
            <li><a target="_blank" href="https://blog.phlok.com/jobs">Jobs</a></li>
            <li><a target="_blank" href="https://phlok.me/contact">Get in touch</a></li>
          </ul>
        </div>
        <div class="col-sx-6 col-sm-3">
          <ul>
            <li><a target="_blank" href="http://blog.phlok.com/">Blog</a></li>
            <li><a target="_blank" href="https://phlok.me/press">Press</a></li>
            <li><a target="_blank" href="https://phlok.me/faq">FAQS</a></li>
            <li><a target="_blank" href="https://phlok.me/downloads">Downloads</a></li>
          </ul>
        </div>
        <div class="col-sm-offset-1 col-sm-5 col-lg-offset-2 col-lg-3">
          <p>Phlok</p>
          <small>Here at Phlok we are constantly evolving and growing, if you have any feedback or suggestions give us a call:</small>
          <ul class="contact-numbers">
            <li><a href="tel:+4402890396200">+44 (0) 2890 396200 NI</a></li>
            <li><a href="tel:+35318464584">+353 (1) 846 4584 ROI</a></li>
          </ul>
        </div>
      </div>

    </div>
    
    <div id="copyright">
      <div class="container center">
        <small>© Copyright 2015. Phlok. All Rights Reserved.<br />
            55-59 Adelaide Street, Belfast, BT2 8FE, Northern Ireland. Business Registration No.NI608035</small>
      </div>
    </div>

	</footer>
    
	<!-- jQuery & Bootstrap Libraries -->
  <script src="js/vendor/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="js/vendor/modernizr.js"></script>
  <script src="js/bootstrap.youtubepopup.min.js"></script>
    
	<!-- Created by Tepee Design -->
  <script src="js/main.js"></script>
  
  </body>
</html>