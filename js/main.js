(function($) {

/*
  LIGHTBOX FORM
*/


  // Light box Functionality
  $(".launch-form").on("click", function(e){

    // prevent default 
    e.preventDefault();

    // if mobile, scroll down to form
    if ($(window).width() > 767 ){

        // scroll to the top of the page
        $('html, body').animate({scrollTop : 0},800);

        // display form
        $("#app-model").delay(800).fadeIn();
    }
    else{

      // display form
      $("#app-model").fadeIn('fast', function(){
        $('html, body').animate({
          scrollTop: $("#app-model").offset().top
        }, 2000);
      });
      
    }

  });

  // Light box Functionality
  $(".close").on("click", function(e){

    // prevent default 
    e.preventDefault();

    // hide the form
    $("#app-model").fadeOut();

  });

/*
  FORM
*/

function validateEmail(email) { 
  // http://stackoverflow.com/a/46181/11236
  
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

//callback handler for form submit
$("#enquiryForm").submit(function(e){

    // colect all input valiues for validation
    var name      = $("#name").val(),
        email     = $("#email").val(),
        tel       = $("#tel").val();

    // reset input errors
    $(".form-group").removeClass("has-error has-feedback");
    $(".form-group span").css("display", "none");
    $("#validation-message").css('display', 'none').removeClass("bg-danger bg-info");

    if(name == ""){
      $("#name").parent(".form-group").addClass("has-error has-feedback");
      $("#name").next("span").css("display", "block");
      return false;
    }

    if( !validateEmail(email) ){
      $("#email").parent(".form-group").addClass("has-error has-feedback");
      $("#email").next("span").css("display", "block").text("your email in invalid");
       return false;
    } 
    else{
      $("#email").next("span").text("Please add a valid email address");
    }

    if(email == ""){
      $("#email").parent(".form-group").addClass("has-error has-feedback");
      $("#email").next("span").css("display", "block");
      return false;
    }

    tel = tel.replace(/[^0-9]/g, '');
    if(tel.length != 11) { 
       $("#tel").parent(".form-group").addClass("has-error has-feedback");
       $("#tel").next("span").css("display", "block").text("There must be 11 numbers");
       return false;
    } 
    else{
      $("#tel").next("span").text("Please add a contact number")
    }

    if(tel == ""){
      $("#tel").parent(".form-group").addClass("has-error has-feedback");
      $("#tel").next("span").css("display", "block");
      return false;
    }

    var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");

    $.ajax({
        url : formURL,
        type: "POST",
        data : postData,
        statusCode: {
          404: function() {
            $("#validation-message").addClass("bg-info").css('display', 'block').text("Contact form not in place.");      
          }
        },
        success:function(data, textStatus, jqXHR) 
        {
          $("#enquiryForm").hide();
          $("#validation-message").addClass("bg-success").css('display', 'block').text("Thankyou, we have received your details and will get in contact with you shortly.");
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
           $("#validation-message").addClass("bg-danger").css('display', 'block').text("Something went wrong. your information was not sent on this ocassion, Please try again.");      
        }
    });

    e.preventDefault(); //STOP default action

});




/*
  STRATEGY TABS
*/

  // set arrow position
  function setArrowPosition(){

    var activeContainer = $("#strategy .container .col-sm-12 .active").attr("id");
    var id = activeContainer.replace("-content", "");
    var target = $("#" + id);

    var firstLink = $(target).position().left;
    $(".arrow").css('left', firstLink + "px");

  } 

  // toggle content
  function toggleContent(target){
    // load content
    $("#strategy-content .content").removeClass("active");
    $(target).addClass("active");
  }


  // load corrent content
  $("#strategy ul li a").on("click", function(e){
    
    // prevent page reloading
    e.preventDefault();
    
    var target = "#" + $(this).attr("id") + "-content";
    var openSection = "#" + $("#strategy .container .col-sm-12 .active").attr("id");
    

    // set drawer status
    $("#strategy-content").toggleClass("open");

    // if the drawer is closed
    if( $("#strategy-content").hasClass("open") ){

      // open drawer
      $("#strategy-content").slideDown();

      // load content
      toggleContent(target);

      // set arrow position
      setArrowPosition();

    }else{

      // if link clicked is already displayed
      if( target == openSection ){

        // close drawer
        $("#strategy-content").slideUp();

        // set arrow position
        setArrowPosition();
        
      }
      else{
        
        toggleContent(target);

        // set arrow position
        setArrowPosition();
        
      }

    }

  });


/*
  Modernizr - Add placeholders to browsers with no support
*/

  if(!Modernizr.input.placeholder) {
      $("input[placeholder]").each(function() {
          var placeholder = $(this).attr("placeholder");

          $(this).val(placeholder).focus(function() {
              if($(this).val() == placeholder) {
                  $(this).val("")
              }
          }).blur(function() {
              if($(this).val() == "") {
                  $(this).val(placeholder)
              }
          });
      });
  }

/*
  RESIZE FUNCTION
*/
  
  $(window).resize(function(){
    setArrowPosition();
  });




})(jQuery);