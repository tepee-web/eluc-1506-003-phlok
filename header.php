<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html> <!--<![endif]-->
<head>
    <meta charset="utf-8">
		
    <!-- Title -->
    <title>Phlok Consumer</title>

    <!-- Pull latest version of IE -->
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!-- Set Viewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- IOS Icons -->
    	<meta name="apple-mobile-web-app-capable" content="yes">
  		<meta name="format-detection" content="telephone=no">
		
  		<!-- 16x16 pixels -->
  		<link rel="shortcut icon" href="favicon.ico">
  		<!-- 32x32 pixels -->
  		<link rel="shortcut icon" href="favicon.png">

  		<!-- 57x57 (precomposed) for iPhone 3GS, 2011 iPod Touch and older Android devices -->
  		<link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-precomposed.png">
  		<!-- 72x72 (precomposed) for 1st generation iPad, iPad 2 and iPad mini -->
  		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
  		<!-- 114x114 (precomposed) for iPhone 4, 4S, 5 and 2012 iPod Touch -->
  		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
  		<!-- 144x144 (precomposed) for iPad 3rd and 4th generation -->
  		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- Stylesheets -->
    <link href="stylesheets/bootstrap.css" rel="stylesheet" media="screen">
    <link href="stylesheets/screen.css" rel="stylesheet" media="screen">

  	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->

</head>
<body>

  <!-- HEADER -->
  <header id="header">
    <div class="container">

      <div class="row">
        <h3>PHLOK for business</h3>
        <ul>
          <li><a target="_blank" href="https://phlok.me/" title="Merchant Login" class="btn btn-default">For Business</a></li>
          <li><a target="_blank" href="https://phlok.me/" title="Get started" class="btn btn-default">Login</a></li>
        </ul>
      </div>

      <div class="row center">
          <img src="img/phlock-logo-large.png" alt="Phlock logo large"/>
          <h1>REAL MONEY REWARDS AT YOUR<br /> FAVOURITE LOCAL BUSINESSES</h1>
          <h4>Collect phlok points and support local businesses while you do!</h4>

          <div class="col-sm-12 center">
            <a target="_blank" href="https://itunes.apple.com/ie/app/phlok/id481715216?mt=8" title="Request a trial" class="btn btn-lg btn-info trial launch-form">Download App</a>
          </div>
      </div>

    </div>
  </header>
