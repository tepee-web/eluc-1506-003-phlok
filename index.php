<?php include('header.php'); ?>
  
<!--
  MAIN SECTION
-->
<section id="main">

    <article class="title">
      <div class="container center">
        <h3>Collect points in one business, spend in another, it’s just like money!</h3>
      </div>
    </article>

    <article id="steps" class="light-grey-bg">
      <div class="container">
        <div class="col-sm-6 col-md-3">
          <div class="step">
            <img src="img/follow.png" alt="Follow"/>
            <h3 class="orange">Follow</h3>
            <p>Collect points when you follow your local businesses and be kept up to date with the latest specials  and new products.</p>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="step">
            <img src="img/checkin.png" alt="Checkin"/>
            <h3 class="orange">Checkin</h3>
            <p>Collect points when you follow your local businesses and be kept up to date with the latest specials  and new products.</p>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="step">
            <img src="img/purchase.png" alt="Purchase"/>
            <h3 class="orange">Purpose</h3>
            <p>Let people know where you are, get rewarded with points when you do!</p>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="step">
            <img src="img/pay.png" alt="Pay"/>
            <h3 class="orange">Pay</h3>
            <p>Attach your debit/credit card and Pay with Phlok all the time, it rewards you and it’s great for the business, no card fees with Phlok you see!</p>
          </div>
        </div>
      </div>
    </article>

    <article id="request-demo-form">
      <div class="container">
        <div class="col-sm-offset-1 col-sm-10 center">
          <h2>Are you a home owner?</h2>
          <h3>Join thousands of independent businesses on Phlok,<br /> a digital strategy that works!</h3>
          <p id="validation-message"></p>
          
          <div class="row left">  
            <form method="POST" name="enquiryForm" id="enquiryForm" action="enquiry.php">

              <div class="col-sm-4">
                <div class="form-group">
                  <label for="name">Full Name</label>
                  <input type="text" class="form-control" id="name" name="name"/>
                  <span class="form-control-feedback" aria-hidden="true">Please add your full name</span>
                </div>
              </div> 
              <div class="col-sm-4">
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" name="email"/>
                  <span class="form-control-feedback" aria-hidden="true">Please add a valid email address</span>
                </div>
              </div> 
              <div class="col-sm-4">
                <div class="form-group">
                  <label for="name">Phone Number</label>
                  <input type="text" class="form-control" id="tel" name="tel"/>
                  <span class="form-control-feedback" aria-hidden="true">Please add a contact number</span>
                </div>
              </div> 
              <div class="col-sm-12 center">
                <button type="submit" class="btn btn-default">Request a Demo</button>
              </div>
            </form>

            
          </div>
        </div>
      </div>
    </article>

    <article id="independants" class="light-grey-bg">
      <div class="container">
        <div class="col-sm-6 col-lg-6">
          <h2 class="orange">Invite your friends</h2>
          <p>Spread the word. Get your friends involved. Who doesn’t like to get free money while discovering hidden gems and great value? Invite your friends to Phlok and get rewarded when they join. Go to Invite Friends in the menu.</p>
        </div>
        <div class="col-sm-6 col-lg-offset-1 col-lg-5">
          <img src="img/mobile-invite-friends.png" alt="invite friends">
        </div>
      </div>
    </article>

    <article id="getting-around" class="title">
      <div class="container center">
        <h2>We are doing important work, they think so too.</h2>
        <ul>
          <li class="sky"><img src="img/sponsors/1.png" alt="Sky News"></li>
          <li class="rte"><img src="img/sponsors/2.png" alt="RTE"></li>
          <li class="irish"><img src="img/sponsors/3.png" alt="Irish Times"></li>
        </ul>
      </div>
    </article>

</section>

<section id="app-model">

  <div class="form-container">
    <a href="" title="Close form" class="close">
      <span class="glyphicon glyphicon-remove-circle"></span>
    </a>
    <h2 class="orange">Download App</h2>
    <p>You can either download our app from <a href="https://itunes.apple.com/ie/app/phlok/id481715216?mt=8">Apple's itunes</a>, or from <a href="https://play.google.com/store/apps/details?id=com.phlok.android&hl=en_GB">Google's Play Store</a>.</p>
    <div class="row">
      <div class="col-xs-6">
        <a target="_blank" class="btn" href="https://itunes.apple.com/ie/app/phlok/id481715216?mt=8"><img class="img-repsonsive" src="img/apple-btn.png" alt=""></a>
      </div>
      <div class="col-xs-6">
        <a target="_blank" class="btn"  href="https://play.google.com/store/apps/details?id=com.phlok.android&hl=en_GB"><img class="img-repsonsive" src="img/play.png" alt=""></a>
      </div>
    </div>
  </div>

</section>


<?php include('footer.php'); ?>